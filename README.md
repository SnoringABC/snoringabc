My name is Cara Gilmore from Birmingham, Alabama and I currently work in the finance department for a large company. In my spare time, I enjoy Barbecuing with my family, cruising about the city and parks, and just relaxing at a cafe with a good book. I was diagnosed with sleep apnea in 2004 as I had already had been exposed to the disorder as many of my family members were already living with the condition for many years.

Address: 300 East 23rd Street, Apt 10E, New York, NY 10010

Phone: 424-345-0340
